local config_path = vim.fn.stdpath('config')
local git_output = vim.fn.system('cd ' .. vim.fn.shellescape(config_path) .. ' && git pull')

if vim.v.shell_error ~= 0 then
  vim.api.nvim_create_autocmd('UiEnter', {
    once = true,
    callback = function()
      if vim.bo.filetype ~= 'gitcommit' then
        vim.notify('Could not auto-update Neovim config:\n' .. git_output)
      end
    end
  })

  require('startup')
else
  if git_output ~= 'Already up to date.\n' then
    if vim.g.config_reloading then
      vim.notify('Config git update keeps loading...\n' .. git_output)
      require('startup')
    else
      vim.g.config_reloading = true
      vim.notify('Config changes loaded, restarting...')
      vim.cmd.source(vim.fn.stdpath('config') .. '/init.lua')
    end
  else
    require('startup')

    if vim.g.config_reloading then
      require('packer').sync()
    end
  end
end
