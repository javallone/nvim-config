local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

local basename = function(str)
  return string.gsub(str, "(.*/)(.*)", "%2")
end

local join_paths = function(...)
  return table.concat({ ... }, '/')
end

local _base_lua_path = join_paths(vim.fn.stdpath('config'), 'lua')

local glob_require = function(package)
  local glob_path = join_paths(
    _base_lua_path,
    package,
    '*.lua'
  )

  local result = {}

  for i, path in pairs(vim.split(vim.fn.glob(glob_path), '\n')) do
    -- convert absolute filename to relative
    -- ~/.config/nvim/lua/<package>/<module>.lua => <package>/foo
    local relfilename = path:gsub(_base_lua_path, ""):gsub(".lua$", "")
    local path_basename = basename(relfilename)
    -- skip `init` and files starting with underscore.
    if (path_basename ~= 'init' and path_basename:sub(1, 1) ~= '_') then
      result[i] = require(relfilename)
    end
  end

  return result
end

return require('packer').startup {
  function(use)
    for _, defn in pairs(glob_require('plugins')) do
      use(defn)
    end

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if packer_bootstrap then
      require('packer').sync()
    end
  end,
  config = {
    display = {
      open_fn = require('packer.util').float,
    }
  }
}
