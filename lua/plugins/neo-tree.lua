-- Neo Tree
return {
  'nvim-neo-tree/neo-tree.nvim',
  branch = 'v3.x',
  requires = {
    'nvim-lua/plenary.nvim',
    'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
    'MunifTanjim/nui.nvim',
  },
  config = function()
    require('neo-tree').setup({
      close_if_last_window = true,
      name = {
        trailing_slash = true,
        use_git_status_colors = true,
        highlight = 'NeoTreeFileName',
      },
      filesystem = {
        follow_current_file = {
          enabled = true,
        }
      }
    })

    vim.api.nvim_create_augroup('neotree', {})
    vim.api.nvim_create_autocmd('UiEnter', {
      desc = 'Open Neotree automatically',
      group = 'neotree',
      callback = function()
        if vim.fn.argc() == 0 then
          vim.cmd.Neotree('toggle')
        end
      end
    })
  end
}
