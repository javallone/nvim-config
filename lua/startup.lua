-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.g.suda_smart_edit = 1

-- [[ Setting options ]]
-- See `:help vim.o`
-- NOTE: You can change these options as you wish!

vim.o.title = true
vim.o.guifont = 'Inconsolata'
--if vim.loop.os_uname().sysname == 'Linux' then
--  vim.g.neovide_scale_factor = 0.66
--end
vim.o.backspace = 'indent,eol,start'
vim.o.laststatus = 2 -- Always show the status line
vim.wo.colorcolumn = '80'

-- Enable spell checking and set language
vim.o.spell = true
vim.o.spelllang = 'en_us'

vim.o.ruler = true      -- Show the ruler in the footer
vim.o.cursorline = true -- Highlight line the cursor is on

vim.o.incsearch = true  -- Search as you type
vim.o.hlsearch = true   -- Set highlight on search
vim.wo.number = true    -- Make line numbers default
vim.o.mouse = 'a'       -- Enable mouse mode

-- Sync clipboard between OS and Neovim.
--  Remove this option if you want your OS clipboard to remain independent.
--  See `:help 'clipboard'`
vim.o.clipboard = 'unnamedplus'
vim.o.fixeol = false     -- Turn off appending new line in the end of a file

vim.o.expandtab = true   -- Use spaces by default
vim.o.shiftwidth = 2     -- Set amount of space characters, when we press '<' or '>'
vim.o.tabstop = 2        -- 1 tab equal 2 spaces
vim.o.smartindent = true -- Turn on smart indentation. See in the docs for more info
vim.o.breakindent = true -- Enable break indent
vim.o.undofile = true    -- Save undo history

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true
vim.o.joinspaces = false  -- Join multiple spaces in search
vim.o.showmatch = true    -- Highlight search instances

vim.wo.signcolumn = 'yes' -- Keep signcolumn on by default

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

vim.o.completeopt = 'menuone,noselect' -- Set completeopt to have a better completion experience
vim.o.termguicolors = true             -- NOTE: You should make sure your terminal supports this

-- Highlight trailing spaces
vim.o.list = true
vim.o.listchars = 'trail:·,tab:  '

-- Folding
vim.o.foldmethod = 'syntax'
vim.o.foldlevelstart = 99 -- Open all folds when opening a buffer

-- Window
vim.o.splitbelow = true -- Put new windows below current
vim.o.splitright = true -- Put new vertical splits to right

vim.cmd.filetype('plugin', 'on')

vim.o.shortmess = vim.o.shortmess .. 'A'
vim.api.nvim_create_autocmd({ 'FocusGained', 'BufEnter' }, {
  desc = 'Autoreload changed files',
  callback = function()
    vim.cmd.checktime()
  end
})

vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  desc = 'Highlight text on yank',
  group = 'YankHighlight',
  callback = function()
    vim.highlight.on_yank()
  end,
})

vim.api.nvim_create_augroup('PackerConfigUpdate', {})
vim.api.nvim_create_autocmd('BufWritePost', {
  desc = 'Reload plugins on config change',
  group = 'PackerConfigUpdate',
  pattern = vim.fn.stdpath('config') .. '/lua/plugins/*.lua',
  callback = function()
    vim.cmd.source(vim.fn.stdpath('config') .. '/lua/plugins/init.lua')
    require('packer').compile()
  end
})

require('plugins')
require('keybindings')
